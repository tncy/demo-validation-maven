package net.tncy.demo.validation;

import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 *
 * @author Xavier Roy
 */
public class Person {

    @NotNull
    @Size(min = 1, message = "Le prénom doit contenir au moins 1 caractère")
    private String firstName;
    @NotNull
    @Size(min = 1)
    private String lastName;
    @NotNull(groups = {AdultCheck.class})
    private Date birthDate;
    private String citizenship;
    @NotNull(groups = {AdultCheck.class})
    @Min(value = 18, groups = {AdultCheck.class})
    private transient Integer age;

    public Person() {
    }

    public Person(String firstName, String lastName) {
        this();
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Person(String firstName, String lastName, Date birthDate, String citizenship) {
        this(firstName, lastName);
        this.birthDate = birthDate;
        this.citizenship = citizenship;
        computeAge();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        computeAge();
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public Integer getAge() {
        return age;
    }

    private void computeAge() {
        if (birthDate != null) {
            LocalDate now = LocalDate.now();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(birthDate);
            int birthYear = calendar.get(Calendar.YEAR);
            int birthMonth = calendar.get(Calendar.MONTH);
            int bithDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            LocalDate birth = LocalDate.of(birthYear, birthMonth, bithDayOfMonth);
            age= Period.between(birth, now).getYears();
        } else {
            age = null;
        }

    }

}
